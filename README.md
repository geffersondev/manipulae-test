
# Manipulaê Automation Test
Simple UI automation project for Manipulaê quality assurance engineer test

## Manual Tests
![Alt text](Manipulae.png)

# Requirements
- Node v16.14.0+
- NPM 8.3.1+
- Yarn 1.22.19+ (optional)

# Libraries
- Cypress
- FakerJS
- cypress-cucumber-preprocessor
- ESLint
- Typescript

# Get started
## Installation

```bash
npm install
```
or
```bash
yarn install
```

# How to run the tests

## Running tests

```bash
npm test
```

```bash
yarn test
```

## Running tagged tests

### Running smoke tests

```bash
npm run tag:smoke
```

```bash
yarn tag:smoke
```

# Running tests manually

```bash
npm run cypress:open
```

```bash
yarn cypress:open
```

## ESLint

### Find Issues

Run ESLint to find problems

```bash
npm run lint:eslint
```

```bash
yarn lint:eslint
```