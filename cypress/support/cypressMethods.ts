export default abstract class CypressMethods {

    protected accessURL(url: string, cleanAppData?: boolean, waitSeconds?: number): void {
        if(cleanAppData){
            cy.clearAllCookies();
            cy.clearAllLocalStorage();
            cy.clearAllSessionStorage();
        }
        cy.visit(url)
        if(waitSeconds)
            cy.wait((waitSeconds * 1000))
    }

    protected sendKeys(element: string, content: string, skipClearField?: boolean){
        if(!skipClearField){
            cy.get(element).clear();
        }
        cy.get(element).type(content);
    }


  }
  