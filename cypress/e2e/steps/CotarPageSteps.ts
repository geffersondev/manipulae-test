import { Then, Given, When } from '@badeball/cypress-cucumber-preprocessor'
import CotarPage from '../pageobjects/CotarPage'
import { faker } from '@faker-js/faker';

const cotarPage = new CotarPage();

Given('User access the manipulae cotar page', () => {
  cotarPage.accessCotarPage("https://webmani-test.manipulae.com.br/cotar")
})

When('User fills the street {string} zip code', (cep: string) => {
  cotarPage.fillCEPField(cep);
})

Then('the user fills the observation {string} field', (observation: string) => {
  cotarPage.fillObservationField(observation);
})

When('the user fills his email field', () => {
  cotarPage.fillEmailField(faker.internet.email({provider: 'yahoo.com'}));
})

Then('the user fills his fullname field', () => {
  cotarPage.fillFullnameField(faker.person.firstName() + ' ' + faker.person.middleName() + ' ' + faker.person.lastName());
})

When('the user fills his phone number field and click share data field', () => {
  cotarPage.fillPhoneField(faker.phone.number("+55219########"));
  cotarPage.clickShareDataField();
})

Then('the user select his prescription and upload to the platform and click send', () => {
  cotarPage.uploadPrescriptionFile();
  cotarPage.clickSendPrescription();
})

Then('the system should return a success message', () => {
  cotarPage.validateSendPrescription();
})

Then('the system should display a invalid region message', () => {
  cotarPage.validateSendInvalidPrescription();
})



