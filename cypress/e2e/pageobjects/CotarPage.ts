import CypressMethods from "../../support/cypressMethods";
import CotarPageMaps from "../uimaps/CotarPageMaps";
import { faker } from '@faker-js/faker';

export default class CotarPage extends CypressMethods {

    cotarPageMaps = new CotarPageMaps();

    public accessCotarPage(url: string){
        this.accessURL(url, true, 2)
    }

    public fillCEPField(content: string){  
        this.sendKeys(this.cotarPageMaps.CEP_FIELD, content, true)
    }

    public fillObservationField(content: string){
        cy.get('body').then((body) => {
            if (body.find(this.cotarPageMaps.OBSERVATION_FIELD).length > 0) {
                this.sendKeys(this.cotarPageMaps.OBSERVATION_FIELD, content, true)
            }
        });
        // this.sendKeys(this.cotarPageMaps.OBSERVATION_FIELD, content, true)
    }

    public fillEmailField(content: string){  
        this.sendKeys(this.cotarPageMaps.EMAIL_FIELD, content, true)
    }

    public fillFullnameField(content: string){  
        this.sendKeys(this.cotarPageMaps.FULLNAME_FIELD, content, true)
    }

    public fillPhoneField(content: string){  
        this.sendKeys(this.cotarPageMaps.PHONE_FIELD, content, true)
    }

    public clickShareDataField(){
        cy.get("label:has( > input[name='aceitaCompartilharDados'])").click()
    }

    public clickSendPrescription(){
        cy.get(this.cotarPageMaps.SEND_PRESCRIPTION_BUTTON).click()
    }

    public validateSendPrescription(){
        cy.wait(4000)
        cy.get('h2').should('contain.text', 'Receita enviada')
        cy.get('h2').should('contain.text', 'com sucesso!')
    }

    public validateSendInvalidPrescription(){
        cy.wait(4000)
        cy.get('h2').should('contain.text', 'Que pena. Ainda não atendemos sua região.')
    }

    public uploadPrescriptionFile(){
        cy.get(this.cotarPageMaps.FILE_UPLOADER_FIELD).selectFile('./cypress/fixtures/' + 'prescription10.jpg', { action: 'drag-drop', force: true })
    }

}
