Feature: New Random Quote Request

  Background:
    Given User access the manipulae cotar page

  @smoke
  Scenario: Send Quote Request with Prescription
    When User fills the street "23900650" zip code
    Then the user fills the observation "Nova receita do meu médico" field
    When the user fills his email field
    Then the user fills his fullname field
    When the user fills his phone number field and click share data field
    Then the user select his prescription and upload to the platform and click send
    Then the system should return a success message

  @smoke
  Scenario: Send Quote Request with Prescription from a invalid region 
    When User fills the street "69945970" zip code
    Then the user fills the observation "Nova receita do meu médico" field
    When the user fills his email field
    Then the user fills his fullname field
    When the user fills his phone number field and click share data field
    Then the user select his prescription and upload to the platform and click send
    Then the system should display a invalid region message