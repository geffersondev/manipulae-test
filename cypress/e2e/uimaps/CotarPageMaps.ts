export default class CotarPageMaps{

    CEP_FIELD = "div:has( > input[name='zip'])";
    OBSERVATION_FIELD = "div:has( > textarea[name='observation'])";
    EMAIL_FIELD = "div:has( > input[name='email'])";
    FULLNAME_FIELD = "div:has( > input[name='name'])";
    PHONE_FIELD = "div:has( > input[name='phone'])";
    SHARE_DATA_FIELD = "label:has( > input[name='aceitaCompartilharDados'])";
    SEND_PRESCRIPTION_BUTTON = "button[type='submit']";
    FILE_UPLOADER_FIELD = "input[data-testid='file-uploader']";
}